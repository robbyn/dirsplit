package org.tastefuljava.dirsplit;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) {
        try {
            String prefix = "";
            int limit = 100;
            int st = 0;
            for (String arg: args) {
                switch (st) {
                    case 0:
                        switch (arg) {
                            case "-p":
                            case "--prefix":
                                st = 1;
                                break;
                            case "-l":
                            case "--limit":
                                st = 2;
                                break;
                            default:
                                split(arg, prefix, limit);
                                break;
                        }
                        break;
                    case 1:
                        prefix = arg;
                        st = 0;
                        break;
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void split(String path, String prefix, int limit)
            throws IOException {
        File dir = new File(path);
        if (!dir.isDirectory()) {
            throw new IOException("Path is not a directory: " + path);
        }
        String[] names = dir.list();
        int div = names.length/limit;
        int pos = 0;
        for (int i = 0; i < div; ++i) {
            pos = moveChunk(dir, names, pos, limit, prefix, i);
        }
        if (pos < names.length) {
            moveChunk(dir, names, pos, names.length-pos, prefix, div);
        }
    }

    public static int moveChunk(File dir, String[] names, int pos, int limit,
            String prefix, int i) throws IOException {
        String dirName = String.format("%s%04d", prefix, i);
        File subdir = new File(dir, dirName);
        if (!subdir.mkdir()) {
            throw new IOException("Cannot create subdir " + dirName);
        }
        for (int j = 0; j < limit; ++j) {
            String name = names[pos++];
            File from = new File(dir, name);
            File to = new File(subdir, name);
            if (!from.renameTo(to)) {
                System.err.println("--- cannot move " + name + " to "
                        + subdir);
            }
        }
        return pos;
    }
}
